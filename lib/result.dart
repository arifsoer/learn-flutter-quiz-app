import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final Function resetHandler;

  Result(this.resultScore, this.resetHandler);

  String get phraseScore {
    String textResult;
    if (resultScore <= 3) {
      textResult = 'You are on fourth level';
    } else if (resultScore <= 9) {
      textResult = 'You are on third level';
    } else if (resultScore <= 15) {
      textResult = 'You are on second level';
    } else {
      textResult = 'You are on first level, Awesome dud!';
    }
    return textResult;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Text(
            phraseScore,
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          FlatButton(
            onPressed: resetHandler,
            child: Text('Reset Quiz'),
            textColor: Colors.blue,
          )
        ],
      ),
    );
  }
}
