import 'package:flutter/material.dart';

import './Question.dart';
import './answer.dart';

class Quiz extends StatelessWidget {
  final List<Map<String, Object>> questions;
  final Function answerQuestion;
  final int questionIndex;

  Quiz({this.questions, this.answerQuestion, this.questionIndex});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Question(
          questions[questionIndex]['questionText'],
        ),
        ...(questions[questionIndex]['answer'] as List<Map<String, Object>>)
            .map((asnwer) {
          return Answer(
            () => answerQuestion(asnwer['score']),
            asnwer['text'],
          );
        }),
      ],
    );
  }
}
